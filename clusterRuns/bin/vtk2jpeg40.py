#!/usr/bin/env python

import vtk
# load VTK extensions
#from libVTKCommonPython import *
#from libVTKGraphicsPython import *
from vtk.util.misc import vtkGetDataRoot
VTK_DATA_ROOT = vtkGetDataRoot()

print "started"
# Graphics & imaging Factory for offscreen rendering
#these are static methods here
graphics_factory = vtk.vtkGraphicsFactory()
graphics_factory.SetOffScreenOnlyMode(1)
graphics_factory.SetUseMesaClasses(1)
imaging_factory = vtk.vtkImagingFactory()
imaging_factory.SetUseMesaClasses(1)

#read vtu file
reader = vtk.vtkXMLUnstructuredGridReader()
reader2 = vtk.vtkXMLUnstructuredGridReader()
reader.SetFileName("roots040-000.vtu")#VTK_DATA_ROOT + "/Data/blow.vtk")
reader2.SetFileName("phosphorus040-000.vtu")#VTK_DATA_ROOT + "/Data/blow.vtk")
reader.Update()
reader2.Update()

#Extract surface
sF=vtk.vtkDataSetSurfaceFilter()
sF.SetInput(reader.GetOutput())
mapper0=vtk.vtkPolyDataMapper()
mapper0.SetInputConnection(sF.GetOutputPort())
sF2=vtk.vtkDataSetSurfaceFilter()
sF2.SetInput(reader2.GetOutput())
mapper2=vtk.vtkPolyDataMapper()
mapper2.SetInputConnection(sF2.GetOutputPort())

#create actor
actor0=vtk.vtkActor() 
actor0.SetMapper(mapper0)
#actor0.SetPosition(0 40 20)
actor2=vtk.vtkActor() 
actor2.SetMapper(mapper2)
actor2.GetProperty().SetOpacity(0.4)

# create a rendering window and renderer
ren = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.SetOffScreenRendering(1)
renWin.AddRenderer(ren)
renWin.SetSize(500,500)

# assign our actor to the renderer
ren.AddActor(actor0)
ren.AddActor(actor2)

#camera position
camera=vtk.vtkCamera()
#camera.SetClippingRange( 14.411, 14411)
camera.SetFocalPoint( 0, -65, 0)
camera.SetPosition( -280, -65, 0)
#camera.SetViewUp( -0.0301976, 0.359864, 0.932516)
#camera.SetViewAngle( 30)
ren.SetActiveCamera(camera)

#interactive viewing (not needed)
#iren = vtk.vtkRenderWindowInteractor()
#iren.SetRenderWindow(renWin)
#iren.Initialize()
#iren.Start()

#renWin is now rendered to an image
w2i=vtk.vtkWindowToImageFilter()
w2i.SetInput(renWin)
#w2i.SetMagnification(2)
w2i.Update()

#image is written to disk
img=vtk.vtkJPEGWriter()
img.SetInputConnection(w2i.GetOutputPort())
img.SetFileName("roots40.jpg")
img.Write()

print "finished"

