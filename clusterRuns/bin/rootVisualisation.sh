#!/bin/bash

#this script will create a dataset and graph from repeated runs

#loop through folder with output
cd ../visualisation
if [ -f $simulationControlParameters ]; then
   for i in $(ls ); do
    #check if this is folder with data
    datafile=$i/job.sh;
    if [ -f $datafile ]; then
      cd $i
      python ../../bin/vtk2jpeg40.py
      cd ..
      cp $i/roots40.jpg ${i}.roots40.jpg
    fi 
   done
else
   echo "Error: replacement simulationControlParameters missing"   
fi
#return from wd dir
cd ../bin

