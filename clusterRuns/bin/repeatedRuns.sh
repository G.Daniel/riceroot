#!/bin/bash

#this script will run simroot with different parameter sets. It simply replaces input files. 

executable="SimRoot"
path2inputfiles="../InputFiles"
inputfile="main-testing.xml"
#note be carefull with parallel processing - you could run out of memory very easily
maxnumberofprocesses=0
path2output="../Output"
path2bin="../bin"
count=0

#support for 3 factors
path2replacementfiles1="../Replacements/plant"
replacefile1="main-testing.xml"
path2replacementfiles2="../Replacements/rcafunction"
replacefile2="templates/plantTemplate.IncludeAerenchyma.xml"
path2replacementfiles3="../Replacements/nlevels"
replacefile3="environmentalParameters/multiplier.xml"
path2replacementfiles4="../Replacements/soil"
replacefile4="environmentalParameters/environmentalParameters.xml"
path2replacementfiles5="../Replacements/precipitation"
replacefile5="environmentalParameters/multiplier2.xml"

function stripExtension(){
   echo ${1%.*}
}

#check output folder exists
if [ -d $path2output ]; then
   #folder exists check if path2Output is empty
   if [ "$(ls -A $path2output)" ]; then
        echo "Take action $path2output is not Empty"
        echo "Press any key to continue, data will be erased. Use ctrl+C to abort"
        read answer
        rm -drf $path2output/*
   fi
else
   mkdir $path2output
fi

for r in rep1  ; do 
#r="rep0"
for i in $(ls $path2replacementfiles1/*.xml); do
for j in $(ls $path2replacementfiles2/*.xml); do
for k in $(ls $path2replacementfiles3/*.xml); do
for l in $(ls $path2replacementfiles4/*.xml); do
for m in $(ls $path2replacementfiles5/*.xml); do
   #strip folder
   ib=$(basename $i)
   jb=$(basename $j)
   kb=$(basename $k)
   lb=$(basename $l)
   mb=$(basename $m)
   #strip extension 
   is=$(echo ${ib%.*})   #stripExtension $i
   js=$(echo ${jb%.*})
   ks=$(echo ${kb%.*})
   ls=$(echo ${lb%.*})
   ms=$(echo ${mb%.*})
   c="${is}_${js}_${ks}_${ls}_${ms}_${r}"
  	ndir=$path2output/$c
	echo "preparing $c"

 	mkdir $ndir;
	mkdir $ndir/InputFiles;
	cp -lr $path2inputfiles/* $ndir/InputFiles ;

	rm -f $ndir/InputFiles/$replacefile1;
	rm -f $ndir/InputFiles/$replacefile2;
	rm -f $ndir/InputFiles/$replacefile3;
	rm -f $ndir/InputFiles/$replacefile4;
	rm -f $ndir/InputFiles/$replacefile5;
	cp -l $path2replacementfiles1/$ib $ndir/InputFiles/$replacefile1;
	cp -l $path2replacementfiles2/$jb $ndir/InputFiles/$replacefile2;
	cp -l $path2replacementfiles3/$kb $ndir/InputFiles/$replacefile3;
	cp -l $path2replacementfiles4/$lb $ndir/InputFiles/$replacefile4;
	cp -l $path2replacementfiles5/$mb $ndir/InputFiles/$replacefile5;

	cd $ndir;
	cp -l ../$path2bin/$executable ./
	cp ../$path2bin/job.sh ./

	qsub -N 'RCAnitrate' ./job.sh & 
	
	cd ../$path2bin;

done;
done;
done;
done;
done;
done;


