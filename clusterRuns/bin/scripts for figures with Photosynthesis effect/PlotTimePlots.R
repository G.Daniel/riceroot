#!/usr/bin/R
#TODO this needs to be improved (nicer looking etc), but is currently proof of concept.
setwd("../Output")
plants=c("bean","maize")

read.table("rootDryWeight.tab",header=T)->dat
read.table("shootDryWeight.tab",header=T)->dat2
read.table("plantDryWeight.tab",header=T)->dat3
read.table("nutrientStressFactor.tab",header=T)->dat4

for(plant in plants){
   #data reading
   T=dat$time
   M=matrix( c(dat[,paste(plant,"_none_18.0",sep="")],
               dat[,paste(plant,"_none_03.0",sep="")],
               dat[,paste(plant,"_all_18.0",sep="")],
               dat[,paste(plant,"_all_03.0",sep="")]),length(T),4,byrow=F)
   M2=matrix( c(dat2[,paste(plant,"_none_18.0",sep="")],
               dat2[,paste(plant,"_none_03.0",sep="")],
               dat2[,paste(plant,"_all_18.0",sep="")],
               dat2[,paste(plant,"_all_03.0",sep="")]),length(T),4,byrow=F)

   #plotting RCA benefit plot
   svg(filename=paste(plant,"DryWeights.svg",sep='_'),width=7,height=7)
   par(mfcol=c(1,1),ps=16,lwd=2,bty='l' ) #one graph with large text
   xmin=min(T,na.rm = T)
   xmax=max(T,na.rm = T)
   ymin=0
   ymax=1.2*max(M,M2,na.rm = T)
   
   #rootLength Plot
   plot(T,M[,1],
         xlab="simulation time (days)",
         ylab="dryweight (g)",
         main=paste("Dryweights for",plant),
         type='l',
         col=1,
         ylim=c(ymin,ymax)
       )
   lines(T,M[,2],col=2)
   lines(T,M[,3],col=1,lwd=4)
   lines(T,M[,4],col=2,lwd=4)
   lines(T,M2[,1],col=1, lty=2)
   lines(T,M2[,2],col=2, lty=2)
   lines(T,M2[,3],col=1, lty=2,lwd=4)
   lines(T,M2[,4],col=2, lty=2,lwd=4)
   #legend(xmin,ymax,legend=c("HP, no RCA","LP, no RCA","HP, RCA","LP, RCA"),col=c(1,2,1,2),lwd=c(2,2,4,4),bty='n',lty=1,xjust=0,y.intersp=1.3)
   legend(xmin,ymax,legend=c("HP","LP"),col=c(1,2),bty='n',lty=1,xjust=0,y.intersp=1.3)
   legend(xmin,0.8*ymax,legend=c("no RCA","RCA"),lwd=c(2,4),bty='n',lty=1,xjust=0,y.intersp=1.3)
   legend(xmin,0.6*ymax,legend=c("root","shoot"),lty=1:2,bty='n',xjust=0,y.intersp=1.3)
   dev.off()
}   
   

   
#   axis(4,at=ymax*(1:8)/8,labels=max(M2,na.rm = T)*(1:8)/8)
dat=dat3
linetypes=c(1:4,6)
svg(filename=paste("RCAbenefitTime.svg",sep='_'),width=14,height=7)
par(mfcol=c(1,2),ps=20,lwd=4,bty='l',mar=c(5,4,4,0) + 0.50  ) #one graph with large text
 
for(plant in plants){
   #data reading
   T=dat$time
   M1=matrix( c(dat[,paste(plant,"_all_03.0",sep="")],
               dat[,paste(plant,"_P.RespOnly_03.0",sep="")],
               dat[,paste(plant,"_RespOnly_03.0",sep="")],
               dat[,paste(plant,"_POnly_03.0",sep="")],
               dat[,paste(plant,"_PhotoOnly_03.0",sep="")]
               ),length(T),5,byrow=F)

   ref=matrix(c(dat[,paste(plant,"_none_03.0",sep="")]),length(T),5)
   M=100*(M1-ref)/ref
   M[ref==0]<-0

   #plotting RCA benefit plot
   xmin=min(T,na.rm = T)
   xmax=max(T,na.rm = T)
   ymin=0
   ymax=1.2*max(M,na.rm = T)
   
   #rootLength Plot
   if(plant == "maize"){
     par(mar=c(5,2,4,2) + 0.50) #one graph with large text
   }
   plot(T,M[,1],
         xlab="simulation time (days)",
         ylab="",
         main=plant,
         type='l',
         lty=linetypes[1],
         ylim=c(ymin,ymax)
       )
   if(plant=="bean"){
             mtext("increase in dryweight due to RCA (%)",2,3)
   }       
   lines(T,M[,2],lty=linetypes[2])
   lines(T,M[,3],lty=linetypes[3])
   lines(T,M[,4],lty=linetypes[4])
   lines(T,M[,5],lty=linetypes[5])
   #legend(xmin,ymax,legend=c("HP, no RCA","LP, no RCA","HP, RCA","LP, RCA"),col=c(1,2,1,2),lwd=c(2,2,4,4),bty='n',lty=1,xjust=0,y.intersp=1.3)
   if(plant == "maize") legend(xmin,ymax,legend=c("All","Respiratory & P benefit","Reduced Respiration","Reduced P content","Photosynthesis"),col=1,bty='n',lty=linetypes,xjust=0,y.intersp=1.3)
}   
   dev.off()

dat=dat4
for(plant in plants){
   #data reading
   T=dat$time
   M1=matrix( c(
               dat[,paste(plant,"_none_03.0",sep="")],
               dat[,paste(plant,"_none_18.0",sep="")],
               dat[,paste(plant,"_all_03.0",sep="")],
               dat[,paste(plant,"_all_18.0",sep="")]
               ),length(T),4,byrow=F)


   M=1-M1

   #plotting RCA stress plot
   svg(filename=paste(plant,"RCANutrientStress.svg",sep='_'),width=7,height=7)
   par(mfcol=c(1,1),ps=16,lwd=2,bty='l' ) #one graph with large text
   xmin=min(T,na.rm = T)
   xmax=max(T,na.rm = T)
   ymin=0
   ymax=1.2*max(M,na.rm = T)
   
   #rootLength Plot
   plot(T,M[,1],
         xlab="simulation time (days)",
         ylab="Nutrient stress (100%)",
         main=paste("Nutrient stress factor for (",plant,")"),
         type='l',
         col=2,
         ylim=c(ymin,ymax)
       )
   lines(T,M[,2],col=1)
   lines(T,M[,3],col=2,lwd=4)
   lines(T,M[,4],col=1,lwd=4)
   legend(xmin,ymax,legend=c("HP, no RCA","LP, no RCA","HP, RCA","LP, RCA"),col=c(1,2,1,2),lwd=c(2,2,4,4),bty='n',lty=1,xjust=0,y.intersp=1.3)
   dev.off()
}   

