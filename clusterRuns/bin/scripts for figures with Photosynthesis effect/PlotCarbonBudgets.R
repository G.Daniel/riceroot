#!/usr/bin/R
#TODO this needs to be improved (nicer looking etc), but is currently proof of concept.
setwd("../Output")
plants=c("maize","bean")
CinDryWeight=0.45

read.table("lastLines.dat",header=T)->dat
#   colors=gray.colors(4)
   colors=c(gray(0.9),gray(0.1),gray(0.9),gray(0.1))
#   colors=c("blue","red","blue","red")
   shading=c(100,100,15,15)

for(plant in plants){
   #build matrix (each column will be a bar)
   #column selection we want here high and low p with and without RCA
   D=dat$data
   M=matrix( c(dat[,paste(plant,"_none_03.0",sep="")],
               dat[,paste(plant,"_all_03.0",sep="")],
               dat[,paste(plant,"_none_18.0",sep="")],
               dat[,paste(plant,"_all_18.0",sep="")]),length(D),4,byrow=F)
   
   #row selection
   #row selection
   MS=matrix(c(M[D=="carbonAllocation2Roots.tab"],
               M[D=="rootRespiration.tab"],
               M[D=="carbonAllocation2Shoot.tab"],
               M[D=="shootRespiration.tab"]),
               4,4,byrow=T)


   #relative barplot
   #MS=(MS/matrix(colSums(MS),4,4,byrow=T))

   #absolute
   svg(filename=paste(plant,"Cbudgets.cumulative.svg",sep='_'),width=7,height=7)
   par(ps=20,lwd=4,mar=c(5,4,4,0) + 0.50)
   ym=1.2*max(colSums(MS))
   barplot(MS,
           #xlim=c(0,9),width=1.3,
           ylim=c(0,ym),
           names.arg=c("no RCA","RCA","no RCA","RCA"),
           xlab="Low P                |                High P",
           ylab="Cumulative carbon spendature (g C)",
           col=colors,
           density=shading
           )
   title(plant)
   legend(0.1,ym,rev(c("root growth","root respiration","shoot growth","shoot respiration")),bty='n',fill=rev(colors),density=rev(shading),y.intersp=1.3)
   dev.off()
}

#rates
for(plant in plants){
   #build matrix (each column will be a bar)
   #column selection we want here high and low p with and without RCA
   D=dat$data
   M=matrix( c(dat[,paste(plant,"_none_03.0",sep="")],
               dat[,paste(plant,"_all_03.0",sep="")],
               dat[,paste(plant,"_none_18.0",sep="")],
               dat[,paste(plant,"_all_18.0",sep="")]),length(D),4,byrow=F)
   
   #row selection
   MS=matrix(c(M[D=="carbonAllocation2Roots.Rates.tab"],
               M[D=="rootRespiration.Rates.tab"],
               M[D=="carbonAllocation2Shoot.Rates.tab"],
               M[D=="shootRespiration.Rates.tab"]),
               4,4,byrow=T)


   #relative barplot
   #MS=(MS/matrix(colSums(MS),4,4,byrow=T))

   #absolute
   svg(filename=paste(plant,"Cbudgets.rates.svg",sep='_'),width=7,height=7)
   par(ps=20,lwd=4,mar=c(5,4,4,0) + 0.50)
   ym=1.2*max(colSums(MS))
   barplot(MS,
           #xlim=c(0,9),width=1.3,
           ylim=c(0,ym),
           names.arg=c("no RCA","RCA","no RCA","RCA"),
           xlab="Low P                |                High P",
           ylab="Carbon spendature (g C/day)",
           col=colors,
           density=shading
          )
   title(plant)
   legend(0.1,ym,rev(c("root growth","root respiration","shoot growth","shoot respiration")),bty='n',fill=rev(colors),density=rev(shading),y.intersp=1.3)
   dev.off()
}
