#!/usr/bin/R
#TODO this needs to be improved (nicer looking etc), but is currently proof of concept.
setwd("../Output")

read.table("rootDryWeight.tab",header=T)->rdw
read.table("shootDryWeight.tab",header=T)->sdw
read.table("plantDryWeight.tab",header=T)->pdw
read.table("rootLength.tab",header=T)->rl
read.table("leafArea.tab",header=T)->la
read.table("tLength.tab",header=T)->prl
read.table("tDryWeight.tab",header=T)->prdw
read.table("nutrientStressFactor.tab",header=T)->nsf
srl=rl/rdw; srl$time.day.<-rl$time
psrl=prl/prdw; psrl$time.day.<-rl$time
lc=la/((prdw/0.94)/prl); psrl$time.day.<-rl$time
srr=sdw/rdw; srr$time.day.<-rl$time
srrl=sdw/rl; srrl$time.day.<-rl$time
#objects to process
datanames=c("rdw","sdw","pdw","rl","la","prl","prdw","srl","psrl","lc","srr","srrl","nsf")
labels=list(rdw="root_dryweight",
           sdw="shoot_dryweight",
           pwd="plant_dryweight",
           rl="root_length",
           la="leaf_area",
           prl="primary_root_length",
           prdw="primary_root_dryweight",
           srl="SRL",
           psrl="primary_root_SRL",
           lc="leafarea_per_rootcrosssectional_area",
           srr="shoot/root ratio (g/g)",
           srrl="shoot/rootlength ratio (g/cm)",
           nsf="phosphorus stress factor")
for(plant in plants){           
selections     =c(paste(plant,"none_18.0",sep='_'),"maize_none_02.0","bean_none_18.0","bean_none_02.0")
selectionlabels=c("maize_none_18.0","maize_none_02.0","bean_none_18.0","bean_none_02.0")
colors=
linetypes=c(1,1,2,2)
for(dn in datanames){
   #label
   label=paste(labels[dn])

   #column selection by removing columns that are not in the list
   d=get(dn)
   x<-d$time.day.
   y<-matrix(0,length(x),length(selections))
   for(i in 1:length(selections)){
      y[,i]<-d[,names(d)==selections[i]]
   }

   #plot parameters
   svg(filename=paste("timeplot",dn,"RCA.svg",sep='_'),width=7,height=7)
   par(mfcol=c(1,1),ps=16,lwd=2,bty='l' ) #one graph with large text

   #plot boundaries
   xmin=min(x,na.rm = T)
   xmax=max(x,na.rm = T)
   ymin=0
   ymax=1.1*max(y, na.rm = T)

   #Plot
   plot(x,y[,1],
         xlab="simulation time (days)",
         ylab=paste(labels[dn]),
         main=paste(label),
         type='l',
         col=colors[1],
         lty=linetypes[1],
         ylim=c(ymin,ymax)
       )
   for(i in 2:length(selections)){
      lines(x,y[,i],col=colors[i],lty=linetypes[i])
   }
   legend(xmin,ymax,legend=selectionlabels,col=colors,bty='n',lty=linetypes,xjust=0,y.intersp=1.3)
   dev.off()
}   
}
