#!/bin/bash
#plots are generated with R
#R --vanilla --silent < PlotBenefit.R > /dev/null
#R --vanilla --silent < PlotCarbonBudgets.R > /dev/null
#R --vanilla --silent < PlotTimePlots.R > /dev/null
#R --vanilla --silent < newGeneralizedTimePlots.R > /dev/null
R --vanilla --silent < barGraphs.R > /dev/null
#svg to png
#cd ../Output
#for i in $(ls *.svg); do
 #check if this is folder with data
# convert $i $i.png
#done
#cd ../bin

